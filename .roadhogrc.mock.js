var _todo = [];
var _index = 1;
let _ = require("lodash");

export default {
  'GET /api/todo': _todo,

  'DELETE /api/todo/:id': (req, res) => {
    var id = _.toNumber(req.params.id);
    var index = _.findIndex(_todo, {id: id});
    if (index == -1) {
      //todo implement not exist case
    }
    _todo.splice(index, 1);
    res.json({status: true});
  },

  'POST /api/todo': (req, res) => {
    var values = req.body;
    values.id = _index++;
    _todo.push(values);
    res.json({status: true});
  }
};
