import React from 'react';
import {Link} from 'dva/router';
import PropTypes from 'prop-types';
import {connect} from 'dva';
import _ from "lodash";

import styles from './IndexPage.css';

import {Timeline, Icon} from 'antd';

import MainLayout from '../components/MainLayout';
import ControlPanel from './ControlPanel';

const IndexPage = ({todo, dispatch}) => {
  const {items} = todo;
  const ControlPanelProps = {
    handleNewTODO(value){
      dispatch({
        type: 'todo/create',
        payload: {title: value}
      });
    }
  };

  function onDelete(id) {
    dispatch({
      type: 'todo/remove',
      payload: id
    });
  }

  function renderItem(item) {
    return (
      <Timeline.Item key={item.id} className={styles.rowItem}>
        <Link to={`/todo/${item.id}`}>{item.title}</Link>
        <Icon className={styles.deleteIcon} type="minus-circle-o"
              onClick={()=>onDelete(item.id)}/>
      </Timeline.Item>);
  }

  return (
    <MainLayout>
      <ControlPanel {...ControlPanelProps} />
      <Timeline className={styles.list}>
        {items && _.map(items, renderItem)}
      </Timeline>
    </MainLayout>
  );
};

IndexPage.propTypes = {
  todo: PropTypes.object,
  dispatch: PropTypes.func
};

export default connect(({todo}) => ({todo}))(IndexPage);
