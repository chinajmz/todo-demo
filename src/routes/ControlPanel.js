import React from 'react';
import PropTypes from 'prop-types';

import {Input} from 'antd';

const ControlPanel = ({handleNewTODO}) => {

  function handlePressEnter(e) {
    handleNewTODO(e.target.value);
    e.target.value = '';
  }

  return (
    <div>
      <Input onPressEnter={handlePressEnter}/>
    </div>
  );
};

ControlPanel.PropTypes = {
  handleNewTODO: PropTypes.func
};

export default ControlPanel;
