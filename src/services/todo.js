/* eslint-disable */
import request from '../utils/request';

export async function query() {
  return request('/api/todo');
}

export async function create(values) {
  return request('/api/todo', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(values)
  });
}

export async function remove(id) {
  return request(`/api/todo/${id}`, {
    method: 'DELETE'
  });
}
