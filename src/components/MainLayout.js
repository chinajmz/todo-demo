import styles from './MainLayout.less'

const MainLayout = ({children})=> {
  return (
    <div className={styles.normal}>
      <h1 className={styles.title}>Yay! Welcome!</h1>
      <div className={styles.container}>
        {children}
      </div>
    </div>
  );
};

export default MainLayout;
