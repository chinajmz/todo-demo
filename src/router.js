import React from 'react';
import { Router, Route } from 'dva/router';
import IndexPage from './routes/IndexPage';
import TodoDetail from './routes/detail/';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Route path="/" component={IndexPage} />
      <Route path="/todo/:id" component={TodoDetail}/>
    </Router>
  );
}

export default RouterConfig;
