import * as todoService from '../services/todo';

export default {

  namespace: 'todo',

  state: {
    items: []
  },

  subscriptions: {
    setup({dispatch, history}) {
      history.listen(location => {
        if (location.pathname === '/') {
          dispatch({type: 'query'});
        }
      });
    }
  },

  effects: {
    *query({payload}, {call, put}) {
      console.log(123123);
      const {data, err} = yield call(todoService.query);
      if (err) {
        throw err;
      }
      yield put({
        type: 'querySuccess',
        payload: data
      });
    },
    *create({payload}, {call, put}){
      const {err} = yield call(todoService.create, {
        ...payload
      });
      if (err) {
        throw err;
      }
      yield put({type: 'query'});
    },
    *remove({payload}, {call, put}){
      const {err} = yield call(todoService.remove, payload);
      if (err) {
        throw err;
      }
      yield put({type: 'query'});
    }
  },

  reducers: {
    querySuccess(state, {payload}){
      return {...state, items: payload};
    }
  }

};
